#!/usr/bin/python3

# Needle-Finder - this program compares the openQA needles and helps to identify
# possible duplications.
#
# Copyright Red Hat
#
# This file is part of os-autoinst-distri-fedora.
#
# os-autoinst-distri-fedora is free software; you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Lukáš Růžička <lruzicka@redhat.com>

import argparse
import cv2 as cv
import glob
import json
import os
import sys

class Progressmeter:
    """ A progress meter. It will zero out upon creation. To obtain the current progress
    and to bump the meter one step higher, run the 'show' method which return the current progress
    and adds 1 to the counter. """
    def __init__(self, total):
        self.counter = 0
        self.total = total

    def show(self):
        try:
            progress = round((self.counter / self.total)*100)
        # Just in case ... we do not want this to crash the entire program.
        except ZeroDivisionError:
            progress = 0
        self.counter += 1
        return progress

def arguments():
    """ Handle the argument parser and return the CLI arguments. """

    parser = argparse.ArgumentParser(description='The needle-finder script to find matching needles.')
    parser.add_argument('-d', '--directory', default='.', help='Specify a directory where needles are stored. When omitted, the current working directory is used.')
    parser.add_argument('-n', '--needle', required=True, help='Specify the needle file you want to test against other needles. You can use both the json and the png version of the needle.')
    parser.add_argument('-e', '--exact', default='96', help='Specify how exact (in percents) the search should be. Lower exactness results in increased number of false positives.')
    parser.add_argument('-s', '--search', default='normal', help='Type of search "normal" search shows all existing tags that fit into the tested screenshot, while the "reverse" search searches for all existing screenshot in which the tested tag can be found.')
    args = parser.parse_args()
    return args


def get_existing_needles(path):
    """ Performs a recursive search for JSON files
    at the PATH location and return a list of paths
    reffering to json files found."""
    needles = glob.glob(os.path.join(path, '**/*.json'), recursive=True)
    return needles

def convert_path(path, typ):
    """ Converts the path between the .json and .png files.
    User can specify the TYP of the conversion, so that they
    always get the output they need.  """
    location, target = os.path.split(path)
    name, ext = os.path.splitext(target)
    if 'json' in typ:
        return os.path.join(location, (name + '.json'))
    elif 'png' in typ:
        return os.path.join(location, (name + '.png'))

def read_metadata(typ, jsonfile, formt="cv"):
    """ Returns the json metadata from the given file and returns the
    type of metadata users specify in the TYP. """
    with open(jsonfile) as source:
        metadata = json.load(source)
    if typ == "area":
        coordinates = []
        areas = metadata['area']
        for area in areas:
            # The area is described with x, y, width and height.
            coord = (area['xpos'], area['ypos'], area['width'], area['height'])
            # However, for the CV2 library we need a different format of
            # y1, y2, x1, x2
            if formt == "cv":
                coord = convert(coord, "cv")
            coordinates.append(coord)
        return coordinates

    elif typ == "tags":
        return metadata['tags']

def convert(area, formt="cv"):
    """ Converts the area coordinates from the openQA version to CV2 version and vice-versa.
    No checks are performed, users have to supply the correct format in order to convert. """
    if formt == "cv":
        # This converts the openQA into CV2
        x, y, w, h = area
        x2 = x + w
        y2 = y + h
        area = (y, y2, x, x2)
    else:
        # This converts the CV2 into openQA
        y, y2, x, x2 = area
        w = x2 - x
        h = y2 - y
        area = (x, y, w, h)
    return area

def create_picture_templates(coordinates, image):
    """ Iterate over the list of coordinates and for each coordinate
    crop the original image to obtain just the coordinated area to create
    a template which can be compared to the existing images in the repository.
    """
    templates = []
    image = cv.imread(image, cv.IMREAD_GRAYSCALE)
    for c in coordinates:
        cropped = image[c[0]:c[1], c[2]:c[3]]
        templates.append(cropped)
    return templates

def compare_with_image(image, template):
    """ Take the template and compare it against the image. Returns
    a probability score as a result. """
    result = cv.matchTemplate(image, template, cv.TM_CCOEFF_NORMED)
    minval, maxval, minloc, maxloc = cv.minMaxLoc(result)
    rounded = round(maxval, 2)
    return rounded

def build_library(existing_needles):
    """ Iterates over the list of existing needles, examines their areas, and
    crops the needle images according to those areas. Then returns the list. """
    tag_library = []
    progress = Progressmeter(len(existing_needles))
    for path in existing_needles:
        needle = convert_path(path, "json")
        coordinates = read_metadata("area", needle, "cv")
        templates = create_picture_templates(coordinates, convert_path(path, "png"))
        libitem = (path, templates)
        tag_library.append(libitem)
        print(f"== Preparing tag library ---> {progress.show()}%", end="\r", flush=True)
    print("== Preparing tag library ---> Finished.")
    return tag_library

def read_scene_image(path):
    """ Read the image and return its grayscale version."""
    if ".json" in path:
        path = convert_path(path, "png")
    image = cv.imread(path, cv.IMREAD_GRAYSCALE)
    return image

def find_existing_tags(library, scene, exact):
    """ Finds all existing areas that have their equivalent on the tested needle. """
    # Storage for results
    matches = []
    # Measure progress
    progress = Progressmeter(len(library))
    # For each item in the area library
    for libitem in library:
        print(f"  == Comparing existing tags with tested needle: {progress.show()}%", end="\r", flush=True)
        # Remember path for each set of small images (templates)
        path, templates = libitem
        # Remember results from the template iteration
        results = []
        # Compare each template with the tested needle and store the result.
        for template in templates:
            result = compare_with_image(scene, template)
            results.append(result)
        # Now we have a list of results for all areas found in the existing needle.
        # We want to keep the only if all of them match, so we are going to test
        # if the minimal results is above the threshold. If it is, we know that
        # all of the areas could be found on the tested needle.
        if min(results) >= exact:
            matches.append(path)
    return matches

def find_tag_in_existing(templates, existing, exact):
    """ Finds the screens in existing needle that could saturate the area from the tested needle. """
    # Iterate over the templates and find possible matches.
    for template in templates:
        # Delete the matches between single templates.
        matches = []
        # Measure progress
        progress = Progressmeter(len(existing))
        # Iterate over the existing needles.
        for path in existing:
            print(f"  == Searching existing needles: {progress.show()}%", end='\r', flush=True)
            # Read the screenshot for that needle.
            scene = read_scene_image(path)
            # Compare the tag from the tested needle (template) with the screenshot
            # from the existing needle.
            result = compare_with_image(scene, template)
            # If the result is above the threshold, append it into the results.
            if result >= exact:
                matches.append(path)
        # We only want to match existing screenshots that would saturate all areas from the tested
        # needle. Therefore we will move the matched screenshots (for one area) into the existing,
        # dropping the non-matching ones and do another pass for the next area.
        # When all areas have been tried, we will have correct results.
        print(f"\n  == Found matches in this pass: {len(matches)}")
        existing = matches[:]
    return matches

def find_same_screens(image, existing, exact):
    """ Finds existing screens that match the tested screenshot, i.e.
    it returns all existing screenshots that are similar to the tested needle."""
    h, w = image.shape
    matches = []
    print(len(existing))
    progress = Progressmeter(len(existing))
    for source in existing:
        print(f"== Serching existing needles: {progress.show()}%", end='\r', flush=True)
        image2 = read_scene_image(convert_path(source, "png"))
        try:
            error = cv.norm(image, image2, cv.NORM_L2)
            similarity = (1-(error/(h*w)))
            if similarity >= exact:
                matches.append(source)
        except cv.error:
            pass
    return matches

def find_same_areas(needle, existing, exact):
    """ Compares the tags in tested needle with the tags in existing needles
    and tries to find a one to one match. """
    # Read areas from the needle, therefore the variables start in n.
    nareas = read_metadata("area", needle)
    print(f"== Found {len(nareas)} areas in the tested needle.")
    # Deal with all areas in the tested needle.
    for narea in nareas:
        matches = []
        progress = Progressmeter(len(existing))
        print(f"\n== Comparing ares: {narea}")
        nx, ny, nwidth, nheight = narea
        # Deal with all existing needles
        for eneedle in existing:
            print(f"== Comparing the needle tags with tags from existing needles: {progress.show()}%", end="\r", flush=True)
            # Exareas are areas comming from the existing needles.
            # All variables that start in e belong to them.
            exareas = read_metadata("area", eneedle)
            # Deal with each area in that needle
            for exarea in exareas:
                ex, ey, ewidth, eheight = exarea
                # Always remember the bigger size for width and height
                if nwidth < ewidth:
                    nwidth = ewidth
                if nheight < eheight:
                    nheight = eheight
                # Get the CV2 type of area.
                # The nwidth and nheight in both lines are correct
                # otherwise we'd get failures!
                nc = convert((nx, ny, nwidth, nheight), "cv")
                ec = convert((ex, ey, nwidth, nheight), "cv")
                # Read the images from both needles.
                nimage = cv.imread(convert_path(needle, "png"), cv.IMREAD_GRAYSCALE)
                eimage = cv.imread(convert_path(needle, "png"), cv.IMREAD_GRAYSCALE)
                # Crop the tested needle and the exsisting needle
                # to compare them.
                ncropped = nimage[nc[0]:nc[1], nc[2]:nc[3]]
                ecropped = eimage[ec[0]:ec[1], ec[2]:ec[3]]
                # Compare the two images
                try:
                    error = cv.norm(ncropped, ecropped, cv.NORM_L2)
                    similarity = (1-(error/(nheight*nwidth)))
                    if similarity >= exact:
                        # Only append the needle if not already added.
                        if eneedle not in matches:
                            matches.append(eneedle)
                except cv.error:
                    pass
            # The upcoming tags should only be compared to the matches
            # from the previous tags.
            existing = matches[:]

    return matches

def listprint(paths):
    """ Takes a list of paths, finds the tags for each file and prints out structured results. """
    # To make sure we only print when we have some data.
    if paths:
        for path in paths:
            print(f"        * {path}")
            # Open the json file on the path and get the "tags" from it.
            tags = read_metadata("tags", path)
            for tag in tags:
                print(f"            - {tag}")

def main():
    # Read the CLI arguments and assign to vars.
    cli = arguments()
    needle = cli.needle
    path = cli.directory
    # Get full path to working directory, when omitted.
    if path == '.':
        path = os.getcwd()
    # Convert the percentage into a decimal value
    # for result comparisons.
    exact = int(cli.exact)/100

    # Get a list of existing needles in the repository (based on json)
    existing = get_existing_needles(path)

    if cli.search == 'area':
        # To find areas for the tested needle.
        scene = read_scene_image(needle)
        library = build_library(existing)
        print(f"== Found existing tags: {len(library)}")
        print("== Testing images ==")
        possible = find_existing_tags(library, scene, exact)
        print(f"== Found possible matches: {len(possible)}")
        listprint(possible)

    elif cli.search == 'compare':
        # To compare entire screens
        tested = read_scene_image(needle)
        print("== Testing images ==")
        possible = find_same_screens(tested, existing, exact)
        print(f"\n== Found possible matches: {len(possible)}")
        listprint(possible)

    elif cli.search == 'detail':
        # To compare areas (probably the most important one)
        tagmatches = find_same_areas(needle, existing, exact)
        print(f"\n== Found tag matches: {len(tagmatches)}")
        listprint(tagmatches)

    elif cli.search == 'screen':
        # To find screens for the tested areas
        areas = read_metadata("area", convert_path(needle, "json"), "cv")
        templates = create_picture_templates(areas, convert_path(needle, "png"))
        print(f"== Found areas and created templates: {len(templates)}")
        existing = get_existing_needles(path)
        possible = find_tag_in_existing(templates, existing, exact)
        print(f"== Found possible matches: {len(possible)}")
        listprint(possible)
    else:
        print("Unknown operation. Select from: 'area', 'compare', 'detail', and 'screen'.")


if __name__ == "__main__":
    main()
