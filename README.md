# needle-finder



## About needle-finder

With `needle-finder`, you can test a newly created needle against the existing needles to see whether the new needle's functionality could be saturated with an that of an existing one.

In test development, needles are created to mark certain areas in the screeshots that are used for assertions or actions. In test scripts, we often want to perform actions on elements, such as buttons, or check that they are present (titles, texts). Often, different test scripts use the same elements to perform actions with. In that case, an existing needle can be reused rather than created from a scratch. 

Reusing the needles is inevitable to keep the needle repository as small as possible with a low level of duplication. On developing systems, the tested elements tend to change breaking the test scripts. To fix this problem, a new version of the needle must be created that is able to match to new status quo. 

With duplication, each such needle must be 'reneedled' separately which needs more effort than when only one needle would be renewed and all affected test scripts would have access to that corrected version immediately.


## Progress

* Full search functionality. The script is able to find existing needles that match the tested needle with high probability of success.
* Limited CLI functions using sys.argv. Proper argument parsing to be added.
* Hardcoded level of fuzziness (95).


## Usage

```
$ ./needle-finder.py <tested_needle> <path-to-needle-repository>
```
